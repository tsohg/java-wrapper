#!/bin/bash

PATH_FOLDER=path_folder_place_holder

source $PATH_FOLDER/config.sh

ADDED_ARGS=$(cat $PATH_FOLDER/$JAVA_WRAPPER_ARGS)

$JAVA_BACKUP_NAME  $ADDED_ARGS $*

#!/bin/bash

source config.sh

#Path to current folder
PATH_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

JAVA_CMD=$(whereis -b  java | tr " " "\n"  | head -2 | tail -1)
JAVA_DIR=$(dirname $JAVA_CMD)
JAVA_NAME=$(basename $JAVA_CMD)

if [ -f "$JAVA_DIR/$JAVA_BACKUP_NAME" ]
then
	echo "Already installed!"
	exit

fi

#Backup the original command
mv $JAVA_CMD $JAVA_DIR/$JAVA_BACKUP_NAME

#Install the wrapper script
install $JAVA_WRAPPER $JAVA_CMD

#Update the path
sed -i "s~path_folder_place_holder~$PATH_FOLDER~" $JAVA_CMD

#!/bin/bash

source config.sh

JAVA_CMD=$(whereis -b  java | tr " " "\n"  | head -2 | tail -1)
JAVA_DIR=$(dirname $JAVA_CMD)

#Path to current folder
if [ ! -f "$JAVA_DIR/$JAVA_BACKUP_NAME" ]
then
	echo "Not installed!"
	exit

fi

mv $JAVA_DIR/$JAVA_BACKUP_NAME $JAVA_CMD 
